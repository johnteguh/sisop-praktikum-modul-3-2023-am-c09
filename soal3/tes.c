#include<stdio.h>
#include<unistd.h>
#include<string.h>
#include <sys/wait.h>

int main()
{   
    int status;
	
	int fd1[2]; 
	int fd2[2];

	char kata1[] = "proses 1";
	char kata2[] = "proses 2";
	pid_t p;


	if (p > 0) {
		char concat_str[100];
		close(fd1[0]);
		write(fd1[1], kata1, strlen(kata1) + 1);
		close(fd1[1]);
        waitpid(p,&status,0);

		close(fd2[1]);
		read(fd2[0], concat_str, 100);
		printf("%s\n", concat_str);
		close(fd2[0]);
	}


	else {
		close(fd1[1]);
		char concat_str[100];
		read(fd1[0], concat_str, 100);
		printf("%s\n", concat_str);

		close(fd2[0]);
		write(fd2[1], kata2, strlen(kata2) + 1);
		close(fd2[1]);
	}

	return 0;
}
