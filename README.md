# sisop-praktikum-modul-3-2023-AM-C09

### Anggota Kelompok C09

| Nama                       | NRP        |
| -------------------------- | ---------- |
| Farrela Ranku Mahhisa      | 5025211129 |
| Yohanes Teguh Ukur Ginting | 5025211179 |
| Shazia Ingeyla Naveeda     | 5025211203 |

<h2>Daftar Isi</h2>
<br>

- [**Soal 1**](#soal-1)
- [**Soal 4**](#soal-2)
  - [Soal A](#soal-2a)
  - [Soal B](#soal-2b-dan-c)
- [**Soal 3**](#soal-3)
- [**Soal 4**](#soal-4)

<br>
## Soal 1

## Soal 2

Fajar sedang sad karena tidak lulus dalam mata kuliah Aljabar Linear. Selain itu, dia tambah sedih lagi karena bertemu matematika di kuliah Sistem Operasi seperti kalian 🥲. Karena dia tidak bisa ngoding dan tidak bisa menghitung, jadi Fajar memohon jokian kalian. Tugas Fajar adalah sebagai berikut.

### Soal 2.A

Membuat program C dengan nama `kalian.c`, yang berisi program untuk melakukan perkalian matriks. Ukuran matriks pertama adalah `4×2` dan matriks kedua `2×5`. Isi dari matriks didefinisikan di dalam code. Matriks nantinya akan berisi angka random dengan rentang pada matriks pertama adalah `1-5` (inklusif), dan rentang pada matriks kedua adalah `1-4` (inklusif). Tampilkan matriks hasil perkalian tadi ke layar.

### Penyelesaian

```c
#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <pthread.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define bar_a 4
#define kol_a 2
#define bar_b 2
#define kol_b 5

int matx_a[bar_a][kol_a];
int matx_b[bar_b][kol_b];
int (*matx_res)[bar_a][kol_b];

pthread_t tid[3];

void* kalimatrix(void *arg){
    int sum;
    for(int i=0; i<bar_a; i++) {
        for(int j=0; j<kol_b; j++) {
            sum = 0;
            for(int k=0; k<bar_b; k++) {
                sum += matx_a[i][k] * matx_b[k][j];
            }
            (*matx_res)[i][j] = sum;
        }
    }
}

void printarr3(){
    for(int i=0;i<bar_a;i++){
        for(int j=0;j<kol_b;j++){
            printf("[%d] ",(*matx_res)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void* isimatriks(void *arg){
    pthread_t id=pthread_self();
    srand(time(NULL));
    if(pthread_equal(id,tid[0])){
        for(int i=0;i<bar_a;i++){
            for(int j=0;j<kol_a;j++){
                matx_a[i][j]=rand() % 5 + 1;
            }
        }
    }

    if(pthread_equal(id,tid[1])){
        for(int i=0;i<bar_b;i++){
            for(int j=0;j<kol_b;j++){
                matx_b[i][j]=rand() % 4 + 1;
            }
        }
    }
}

int main (void){
    int err;
    for(int i=0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&isimatriks,NULL);
    }
    for(int i=0;i<2;i++){
        pthread_join(tid[i],NULL);
    }

    key_t key = 5557;
    int shmid = shmget(key, sizeof(int[bar_a][kol_b]), IPC_CREAT | 0666);
    matx_res = shmat(shmid, NULL, 0);
    pthread_create(&(tid[0]),NULL,&kalimatrix,NULL);
    pthread_join(tid[0],NULL);
    printf("\n");
    printarr3();
}

```
### Penjelasan

```c
#define bar_a 4
#define kol_a 2
#define bar_b 2
#define kol_b 5

int matx_a[bar_a][kol_a];
int matx_b[bar_b][kol_b];
int (*matx_res)[bar_a][kol_b];

pthread_t tid[3];
```

- Diawal di deklarasikan untuk ukuran matrix pertama dan matrix kedua lalu membuat matrix untuk menyimpan hasil perkalian matrix pertama dan matrix kedua lalu matrix hasil tersebut di deklarasikan sebagai pointer agar dapat disimpan dishared memory untuk petunjuk dan ketentuan soal berikutnya `(*matx_res)[bar_a][kol_b]`
- Dan juga mendeklarasikan penyimpan tid untuk pembuatan thread nantinya `pthread_t tid[3]`

```c
void* kalimatrix(void *arg){
    int sum;
    for(int i=0; i<bar_a; i++) {
        for(int j=0; j<kol_b; j++) {
            sum = 0;
            for(int k=0; k<bar_b; k++) {
                sum += matx_a[i][k] * matx_b[k][j];
            }
            (*matx_res)[i][j] = sum;
        }
    }
}
```

- Lalu akan dibuat fungsi kalimatrix untuk mengalikan matrix pertama dan matrix kedua dengan melakukan perulangan lalu hasil perkalian disimpan dalam `sum` dan di assign kedalam `matx_res`

```c
void printarr3(){
    for(int i=0;i<bar_a;i++){
        for(int j=0;j<kol_b;j++){
            printf("[%d] ",(*matx_res)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
```

- Akan ada fungsi `printarr3()` untuk melakukan output atau print `matx_res` ke terminal sesuai ketentuan soal

```c
void* isimatriks(void *arg){
    pthread_t id=pthread_self();
    srand(time(NULL));
    if(pthread_equal(id,tid[0])){
        for(int i=0;i<bar_a;i++){
            for(int j=0;j<kol_a;j++){
                matx_a[i][j]=rand() % 5 + 1;
            }
        }
    }

    if(pthread_equal(id,tid[1])){
        for(int i=0;i<bar_b;i++){
            for(int j=0;j<kol_b;j++){
                matx_b[i][j]=rand() % 4 + 1;
            }
        }
    }
}
```
- Lalu akan ada fungsi `isimatriks` yang digunakan untuk mengisi matriks pertama dan matriks kedua dengan pengisian tiap index bernilai random dengan interval 1-5 untuk matriks pertama dan 1-4 untuk matriks kedua
- Diawal akan dibuat seed untuk random angka dengan `srand(time(NULL))` yang digunakan agar seed saat pembuatan angka random selalu beda saat digunakan karena untuk pembuatan seednya tergantung detik dengan `time(NULL)`
- `pthread_t id=pthread_self()` digunakan untuk mendapatkan id thread saat ini lalu didalam pengkondisian akan dilakukan cek id thread yang menjalankan fungsi dan dicek ke `tid[]` yang menyimpan thread id saat pembuatan thread di main. Apabila saat pengecekan sama dengan index 0 maka akan mengisi matriks pertama apabila sama dengan index 1 maka mengisi matriks kedua

```c
int main (void){
    int err;
    for(int i=0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&isimatriks,NULL);
    }
    for(int i=0;i<2;i++){
        pthread_join(tid[i],NULL);
    }

    key_t key = 5557;
    int shmid = shmget(key, sizeof(int[bar_a][kol_b]), IPC_CREAT | 0666);
    matx_res = shmat(shmid, NULL, 0);
    pthread_create(&(tid[0]),NULL,&kalimatrix,NULL);
    pthread_join(tid[0],NULL);
    printf("\n");
    printarr3();
}
```

```c
int err;
    for(int i=0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&isimatriks,NULL);
    }
    for(int i=0;i<2;i++){
        pthread_join(tid[i],NULL);
    }
```
- Dilakukan pembuatan thread dengan pthread_create dimana akan dibuat 2 thread dimana thread pertama akan mengisi matriks pertama dan thread kedua akan mengisi matriks kedua 
- Lalu akan dilakukan `pthread_join` untuk wait thread yang berjalan sebelum code melanjutkan ke kode selanjutnya

```c
    key_t key = 5557;
    int shmid = shmget(key, sizeof(int[bar_a][kol_b]), IPC_CREAT | 0666);
    matx_res = shmat(shmid, NULL, 0);
    pthread_create(&(tid[0]),NULL,&kalimatrix,NULL);
    pthread_join(tid[0],NULL);
    printf("\n");
    printarr3();
```
- `key_t key = 5557` dideklarasikan key untuk shared memory dimana key seperti kunci untuk shared memory yang digunakan
- ``int shmid = shmget(key, sizeof(int[bar_a][kol_b]), IPC_CREAT | 0666)`` Mendapatkan id untuk shared memory dengan key yang sudah dibuat, lalu memberikan ukuran shared memory dimana ukurannya adalah matriks 2 dimensi lalu akan dibuat shared memory dengan `IPC_CREAT | 0666`
- Lalu `matx_res = shmat(shmid, NULL, 0)` Mengassign shared memory ke dalam `matx_res` sehingga digunakan untuk menjadikan `matx_res` sebagai shared memory dimana apabila terjadi perubahan, isi di shared memory juga akan berubah

```c
pthread_create(&(tid[0]),NULL,&kalimatrix,NULL);
    pthread_join(tid[0],NULL);
    printf("\n");
    printarr3();
```
- lalu akan membuat thread dimana digunakan untuk mengalikan matriks dan lalu akan di join sebelum akhirnya menjalankan perintah berikutnya
- `printarr3()` Digunakan untuk mengeluarkan hasil dari perkalian matriks

![kalian.png](./img/kalian.jpg)

### Soal 2.B dan C
- Buatlah program C kedua dengan nama cinta.c. Program ini akan mengambil variabel hasil perkalian matriks dari program kalian.c (program sebelumnya). Tampilkan hasil matriks tersebut ke layar.
**(Catatan: wajib menerapkan konsep shared memory)**

