#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <pthread.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <time.h>

#define bar_a 4
#define kol_b 5
int (*matx_res)[bar_a][kol_b];
unsigned long long faktor[bar_a*kol_b];

void isi1(){
    for (int i = 0; i < 20; i++) {
        faktor[i] = 1;
    }
}

void printarr3(){
    for(int i=0;i<bar_a;i++){
        for(int j=0;j<kol_b;j++){
            printf("[%d] ",(*matx_res)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

int main(){

    clock_t start, end;
    double cpu_time_used;

    // Start time
    start = clock();

    key_t key = 5557;
    int thread_args[20];
    int shmid = shmget(key, sizeof(int[bar_a][kol_b]), 0666);
    matx_res = shmat(shmid, NULL, 0);
    printarr3();
    isi1();

    for(int i=0;i<20;i++){
        int n =  i;
            for (int i = 1; i <= (*matx_res)[n/5][n%5]; i++) {
                faktor[n] *= i;
            }
    }

    printf("Faktorial = ");
    for(int i=0;i<20;i++){
        printf("%llu ",faktor[i]);
    }
    printf("\n");
    // shmdt(matx_res);
    // shmctl(shmid, IPC_RMID, NULL);

    // End time
    end = clock();

    // Calculate CPU time used
    cpu_time_used = ((double) (end - start)) / CLOCKS_PER_SEC;

    // Print CPU time used
    printf("CPU time used: %f seconds\n", cpu_time_used);



}
