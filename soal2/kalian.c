#include<stdio.h>
#include<unistd.h>
#include<stdlib.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <pthread.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/shm.h>

#define bar_a 4
#define kol_a 2
#define bar_b 2 
#define kol_b 5

int matx_a[bar_a][kol_a];
int matx_b[bar_b][kol_b];
int (*matx_res)[bar_a][kol_b];

pthread_t tid[3];

void* kalimatrix(void *arg){
    int sum;
    for(int i=0; i<bar_a; i++) {
        for(int j=0; j<kol_b; j++) {
            sum = 0;
            for(int k=0; k<bar_b; k++) {
                sum += matx_a[i][k] * matx_b[k][j];
            }
            (*matx_res)[i][j] = sum;
        }
    }
}

void printarr3(){
    for(int i=0;i<bar_a;i++){
        for(int j=0;j<kol_b;j++){
            printf("[%d] ",(*matx_res)[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}

void* isimatriks(void *arg){
    pthread_t id=pthread_self();
    srand(time(NULL));
    if(pthread_equal(id,tid[0])){
        for(int i=0;i<bar_a;i++){
            for(int j=0;j<kol_a;j++){
                matx_a[i][j]=rand() % 5 + 1;
            }
        }
    }

    if(pthread_equal(id,tid[1])){
        for(int i=0;i<bar_b;i++){
            for(int j=0;j<kol_b;j++){
                matx_b[i][j]=rand() % 4 + 1;
            }
        }
    }

}

int main (void){
    int err;
    for(int i=0;i<2;i++){
        err=pthread_create(&(tid[i]),NULL,&isimatriks,NULL);
    }
    for(int i=0;i<2;i++){
        pthread_join(tid[i],NULL);
    }

    key_t key = 5557;
    int shmid = shmget(key, sizeof(int[bar_a][kol_b]), IPC_CREAT | 0666);
    matx_res = shmat(shmid, NULL, 0);
    pthread_create(&(tid[0]),NULL,&kalimatrix,NULL);
    pthread_join(tid[0],NULL);
    printf("\n");
    printarr3();
}

