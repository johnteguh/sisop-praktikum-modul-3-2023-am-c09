#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<time.h>
#include <sys/wait.h>
#include<sys/prctl.h>
#include<stdbool.h> 
#include <pthread.h>

pid_t pid;
int status;

void donlot(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("wget","wget","https://drive.google.com/u/0/uc?id=1rsR6jTBss1dJh2jdEKKeUyTtTRi_0fqp&export=download","-O","hehe.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

void uzip(){
    pid=fork();
    if(pid<0){
        exit(EXIT_FAILURE);
    }else if(pid==0){
        execlp("unzip","unzip","hehe.zip",NULL);
    }else{
        waitpid(pid, &status,0);
    }
}

int main(int argc, char *argv[]){
    donlot();
    uzip();
}