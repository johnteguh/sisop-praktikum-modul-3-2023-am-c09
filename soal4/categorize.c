#include<signal.h>
#include<sys/types.h>
#include<sys/stat.h>
#include<stdio.h>
#include<stdlib.h>
#include<fcntl.h>
#include<errno.h>
#include<unistd.h>
#include<syslog.h>
#include<string.h>
#include<time.h>
#include <sys/wait.h>
#include<sys/prctl.h>
#include<stdbool.h>
#include <pthread.h>
#include <dirent.h>
#define MAX_LENGTH 1024

pthread_t tid[10];
int jumlah_max;
char *fold_other = "other";
char *fold_categorized = "categorized";
typedef struct{
    char **nama;
    int angka;
} jenis_ext;

jenis_ext nama_ext;
// jenis_ext nama_ext = { .nama = NULL, .angka = 0 };

void* baca_file(void* arg){\
    char* extensi = (char*) arg;
    char *buffer=malloc(MAX_LENGTH+2);
    FILE* file = fopen(extensi, "r");
    struct dirent *ent;
    int count_line = 1;
    // nama_ext.nama = malloc((MAX_LENGTH+1) * sizeof(char*));
    // for (int i = 0; i < MAX_LENGTH+1; i++) {
    //     nama_ext.nama[i] = malloc((MAX_LENGTH+1) * sizeof(char));
    // }

    if (file == NULL) {
        printf("Gagal melakukan baca_file untuk membaca file %s\n", extensi);
        exit(EXIT_FAILURE);
    }

    nama_ext.nama = malloc((count_line+3) * sizeof(char*));

    
    while(fgets(buffer, MAX_LENGTH, file)!=NULL){
        // nama_ext.nama = malloc((count_line+3) * sizeof(char*));
        nama_ext.nama = realloc(nama_ext.nama, (count_line+1) * sizeof(char*));
        nama_ext.nama[count_line] = malloc(strlen(buffer)+2);
        buffer[strcspn(buffer, "\n\r")] = '\0'; // hapus karakter '\n'dan carriage return (\r) dari line
        printf("ext = %s\n",extensi);
        strcpy(nama_ext.nama[count_line], buffer);
        nama_ext.angka=count_line;
        count_line++;
    }
    fclose(file);

}

void* buatdir(void* arg){
    char* extensi = (char*) arg;
    printf("buat dir = %s\n",extensi);
    mkdir(extensi, 0777);
}

int main(){
    //Membaca jenis extension di file extensions.txt dan menyimpan banyak extensionnya
    char *list_extension = "extensions.txt";
    pthread_create(&tid[0],NULL,baca_file,(void *)list_extension);
    pthread_join(tid[0],NULL);
    for(int i=1;i<=nama_ext.angka;i++){
        printf("ext = %s\n",nama_ext.nama[i]);
    }
    printf("berapa line = %d\n",nama_ext.angka);


    //Membuat foldernya
    nama_ext.nama[0] = malloc(strlen(fold_other) +strlen(fold_categorized) + 3);
    strcpy(nama_ext.nama[0],fold_other);
    buatdir((void *)fold_categorized);
    // chdir(fold_categorized);
    for(int i=0;i<=nama_ext.angka;i++){
        char *buffer=malloc(MAX_LENGTH+2);
        sprintf(buffer,"%s/%s",fold_categorized,nama_ext.nama[i]);
        sprintf(buffer,"%s/%s",fold_categorized,nama_ext.nama[i]);
        nama_ext.nama[i] = realloc(nama_ext.nama[i],strlen(buffer)+(5));

        printf("buffer = %s\n",buffer);
        // sprintf(nama_ext.nama[i],"%s/%s",fold_categorized,nama_ext.nama[i]);
        strcpy(nama_ext.nama[i],buffer);
        printf("nama_ext.nama[i] = %s\n",nama_ext.nama[i]);
        pthread_create(&tid[i],NULL,buatdir,(void *)nama_ext.nama[i]);
        free(buffer);
    }
    for(int i=0;i<=nama_ext.angka;i++){
        pthread_join(tid[i],NULL);
    }
    // chdir("..");

    //Membaca max.txt 
    char *file_max = "max.txt";
    FILE *max = fopen(file_max,"r");
    fscanf(max,"%d",&jumlah_max);
    printf("max = %d\n",jumlah_max);
    fclose(max);


    
}
